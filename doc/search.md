# Search

GitLab Docs supports two search backends, Elasticsearch and Lunr.js. Environment variables are passed at
build time to set the search backend.

- The primary production site, docs.gitlab.com, runs [Elasticsearch](https://www.elastic.co/elasticsearch).
- Lunr.js. Archives, review apps, and self-hosted sites run Lunr.js.

## Elasticsearch

The Technical Writing team introduced Elasticsearch in Q3 2024 (see [epic](https://gitlab.com/groups/gitlab-org/-/epics/14747)).

### Implementation details

#### Infrastructure

The Elasticsearch instance for GitLab Docs runs on Elastic Cloud, alongside deployments for
GitLab.com. This is managed by GitLab infrastructure.

The deployment for GitLab Docs is labeled `gitlab-docs-website`.

Within our deployment, we have two indexes:

- `search-gitlab-docs-hugo`: Indexes Hugo site content via the web crawler.
- `search-gitlab-docs-nanoc`: Indexes Nanoc site content via the web crawler.

These both have the same configuration:

- Site maps: `sitemap.xml`.
- Crawl rules: Disallow crawls for the homepage, the search page, and the 404 page.
- Scheduling: Crawl hourly.

If you need to access the Elastic Cloud admin console, you will need to file an Access Request
([example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/31957)).

In addition to the indexes, the admin console is also used for:

- API key management
- Configuring CORS settings for authentication from the website. See
[Enabling CORS](https://www.elastic.co/docs/current/search-ui/tutorials/elasticsearch#enabling-cors)
for more information.

##### Admin links

- [Elastic admin](https://gitlab-docs-website.kb.us-central1.gcp.cloud.es.io:9243/app/home#/)
- [Deployment health](https://cloud.elastic.co/deployments/6812a4f2d673478cabffaf43ffbaab56/health)
- [Logs and metrics](https://cloud.elastic.co/deployments/6812a4f2d673478cabffaf43ffbaab56/logs-metrics)
- [GitLab Docs search indices](https://gitlab-docs-website.kb.us-central1.gcp.cloud.es.io:9243/app/enterprise_search/content/search_indices)

##### Manage API keys

Guidelines for managing keys:

- IMPORTANT: Keys must be read-only and limited in access to specific indexes.
See [Setting Up a Read-only API Key](https://www.elastic.co/docs/current/search-ui/tutorials/elasticsearch#setting-up-a-read-only-api-key)
for the required configuration.
- Issue individual API keys for developers who need to run queries locally. Set a one
year expiration date for developer keys.
- Rotate the production key periodically.

As this API key is used in frontend code, it is a public key, but it is still
good practice to use separate keys and rotate them regularly.

##### Authentication

We use the following environment variables to connect to Elasticsearch:
`ELASTIC_KEY`, `ELASTIC_INDEX`, and `ELASTIC_CLOUD_ID`.

These are passed to the production site and review apps via CI variables.
GitLab Docs maintainers can change the values of these by navigating to Settings > CI/CD  > Variables
in the [GitLab Docs](https://gitlab.com/gitlab-org/gitlab-docs/) project.

#### Frontend

Search forms are built with GitLab UI form components.
The forms make [API requests](../content/frontend/services/elastic_search_api.js) to the
[Elasticsearch API](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html).

Search results appear beneath the form as you type on the homepage and interior pages.
They can also choose "View all results" to run their query from the [advanced search page](https://docs.gitlab.com/search).

The advanced search page includes all results in a paginated list view.
These are rendered alongside custom filters that use data from the
[section metatag](../layouts/head.html) to filter pages by navigation section.

##### Vue components

- [Search forms on the homepage and interior content pages](../content/frontend/search/components/elastic_search_form.vue)
- [Search form and results on the advanced search page](../content/frontend/search/components/elastic_results.vue)
- [Search filters on the advanced search page](../content/frontend/search/components/search_filters.vue)

### Analytics

We'll need to set up analytics on our end.
See this [issue](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/issues/127) for status
on this work.

## Lunr.js Search

[Lunr.js](https://lunrjs.com/) is available as an alternative search backend for archived and self-hosted GitLab Docs installations. Lunr search can also be used in offline or air-gapped environments. Lunr search requires an additional build step to create a search index.

Documentation review apps and versions of the site running on <https://archives.docs.gitlab.com> use Lunr search by default.

## Development

### Local environment

You can build your local Nanoc site to use a specific search backend by setting the `SEARCH_BACKEND` environment variable at compile time.

- Use Elastic search: `SEARCH_BACKEND="elastic" make compile`. Default if `SEARCH_BACKEND` is not set.
- Use Lunr.js search: `SEARCH_BACKEND="lunr" make compile`.

#### Local build with Elasticsearch

Querying Elasticsearch from your local environment requires the following environment variables:
`ELASTIC_KEY`, `ELASTIC_INDEX`, and `ELASTIC_CLOUD_ID`.

Note that work on search UI components should be doable without Elastic access.

1. Request an API key by creating an issue in the `gitlab-docs` project.
1. Copy the `ELASTIC_INDEX` and `ELASTIC_CLOUD_ID` values from the project [CI/CD settings](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/settings/ci_cd).
1. Add all three environment variables to `~/.zshrc` (or whatever your shell settings file is) like this:

    ```shell
    export ELASTIC_INDEX="search-gitlab-docs-nanoc"
    export ELASTIC_CLOUD_ID="xyz987"
    export ELASTIC_KEY="abc123"
    ```

1. Restart your terminal, then run  `make view` to build and preview locally.

Alternatively, you can pass variables into the build like this:

```shell
ELASTIC_KEY="abc123" SEARCH_BACKEND="elastic" make view
```

#### Local build with Lunr.js search

To create a local build with Lunr.js search:

1. `make setup`.
1. `SEARCH_BACKEND="lunr" make compile`.

### Review apps

To build a review app with Elasticsearch as the search backend, include the string `elastic` in the name of your Git branch.
