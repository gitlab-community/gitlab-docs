/* global Vue */
import ElasticResults from './components/elastic_results.vue';
import { activateKeyboardShortcut } from './search_helpers';

document.addEventListener('DOMContentLoaded', () => {
  activateKeyboardShortcut();

  (() =>
    new Vue({
      el: '.js-elastic-search',
      components: {
        ElasticResults,
      },
      render(createElement) {
        return createElement(ElasticResults);
      },
    }))();
});
