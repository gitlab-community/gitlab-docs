/* global Vue */
import ElasticSearchForm from './components/elastic_search_form.vue';
import { activateKeyboardShortcut } from './search_helpers';

document.addEventListener('DOMContentLoaded', () => {
  activateKeyboardShortcut();

  (() =>
    new Vue({
      el: '.js-elastic-search-form',
      components: { ElasticSearchForm },
      render(createElement) {
        return createElement(ElasticSearchForm, {
          props: {
            numResults: 7,
          },
        });
      },
    }))();
});
