# Vendored GitLab UI CSS utils

These are the legacy GitLab UI CSS utilities. We are vendoring them here because we have removed the
legacy CSS mixins library from GitLab UI, but `gitlab-docs` still depends on them. We would normally
migrate to Tailwind CSS, but given that `gitlab-docs` is on maintenance mode until it gets replaced
by the Hugo docs site where Tailwind _is_ set up, we are going for a more boring solution here.
